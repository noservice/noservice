(ns noservice.test.util
  (:require
   [clojure.test :refer [deftest is]]
   [murphy :refer [with-final]]
   [noservice.config :refer [configured-notmuch]]
   [noservice.util :as util :refer [rm-rf]])
  (:import
   (java.io File)
   (java.lang AutoCloseable)
   (java.nio.file Files Path)
   (java.nio.file.attribute FileAttribute)))

(defn get-path [part & parts]
  (Path/of part (into-array String parts)))

(defprotocol AsPath
  (as-path [x]))

(extend-protocol AsPath
  File
  (as-path [f] (.toPath f))
  String
  (as-path [s] (get-path s)))

(defn temp-dir
  ([parent prefix] (temp-dir parent prefix (into-array FileAttribute [])))
  ([parent prefix attrs]
   (-> (as-path parent)
       (Files/createTempDirectory prefix (into-array FileAttribute attrs))
       .toAbsolutePath)))

(defn make-disposition [filename filename*]
  {:headers {"Content-Disposition"
             (format "inline; filename=\"%s\"; filename*='UTF-8'%s;"
                     filename filename*)}})

(deftest content-disposition
  (is (= (util/content-disposition {} "foo")
         (make-disposition "foo" "%66%6f%6f")))
  (is (= (util/content-disposition {} "\n")
         (make-disposition "_" "%0a")))
  (is (= (util/content-disposition {} "\r")
         (make-disposition "_" "%0d")))
  (is (= (util/content-disposition {} "\"")
         (make-disposition "_" "%22"))))

(def notmuch-command-path
  (if (-> "dev/bin/notmuch" File. .exists)
    "dev/bin/notmuch"
    "notmuch"))

(defrecord ^:private NotmuchSandbox
    [dir config]
  AutoCloseable
  (close [x] (rm-rf (:dir x))))

(defn notmuch-test-config-text [root]
  (let [root (str root)]
    (assert (re-matches #"[-_./a-zA-Z0-9]+" root))
    (format "[database]
path=%s

[user]
name=noservice
primary_email=noservice@example.org

[new]
tags=unread;inbox;
ignore=

[search]
exclude_tags=deleted;spam;

[maildir]
synchronize_flags=true
"
            root)))

(defn notmuch-sandbox [parent]
  (with-final [^Path tmpdir (temp-dir parent "notmuch-sandbox-") :error rm-rf
               config (.resolve tmpdir "notmuch-config")
               mail (.resolve tmpdir "mail")
               nm-cmd [notmuch-command-path "--config" (str config)]
               ;; FIXME: hack
               notmuch (configured-notmuch [{:command {:notmuch nm-cmd}}])]
    (Files/createDirectory mail (into-array FileAttribute []))
    (spit (.toFile config)
          (notmuch-test-config-text (.resolve tmpdir "mail")))
    (notmuch "new")
    (->NotmuchSandbox tmpdir config)))
