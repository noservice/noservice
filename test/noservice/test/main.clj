(ns noservice.test.main
  (:require
   [clojure.java.io :refer [delete-file]]
   [clojure.test :refer [deftest is use-fixtures]]
   [buddy.hashers :as hashers]
   [murphy :refer [with-final]]
   [noservice.config :as config]
   [noservice.main :as main]
   [noservice.test.util :refer [temp-dir]])
  (:import
   [java.io File]
   [java.nio.file Files Path]))

(use-fixtures :once (fn [f] (f) (binding [*out* *err*] (flush)) (flush)))

(deftest passwd
  (with-final [^Path tmpdir (temp-dir "target" "test-passwd") :always Files/delete
               pwfile (File. (.toFile tmpdir) "passwd") :always #(delete-file % true)]
    (try
      (with-redefs [main/read-password #(identity "foo")]
        (is (zero? (main/passwd (.getAbsolutePath pwfile))))
        (is (hashers/check "foo" (slurp pwfile))))
      (finally (delete-file pwfile true)))
    (with-redefs [main/read-password (let [x (atom 0)]
                                       #(str (swap! x inc)))]
      (is (not (zero? (main/passwd (.getAbsolutePath pwfile))))))))

(deftest server-arguments
  (let [process-server-args #'main/process-server-args
        get-cc #(config/get-in (:config %) [:client-certs])]
    (is (get-cc (process-server-args [])))
    (is (get-cc (process-server-args ["--client-certs"])))
    (is (= false (get-cc (process-server-args ["--no-client-certs"]))))))
