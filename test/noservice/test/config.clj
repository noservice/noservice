(ns noservice.test.config
  (:require
   [clojure.test :refer [deftest is]]
   [noservice.config :as config]))

(deftest test-get-in
  (is (nil? (config/get-in [] [])))
  (is (= :b (config/get-in [] [] :b)))

  (is (nil? (config/get-in [] [:a])))
  (is (= :b (config/get-in [] [:a] :b)))

  (is (nil? (config/get-in [{}] [:a])))
  (is (= :b (config/get-in [{}] [:a] :b)))

  (is (nil? (config/get-in [{:a :b}] [:c])))
  (is (= :d (config/get-in [{:a :b}] [:c] :d)))
  (is (= :b (config/get-in [{:a :b}] [:a])))
  (is (= :b (config/get-in [{:a :b}] [:a] :d)))

  (is (nil? (config/get-in [{:a 1} {:b 2}] [:c])))
  (is (= 1 (config/get-in [{:a 1} {:b 2}] [:a])))
  (is (= 2 (config/get-in [{:a 1} {:b 2}] [:b])))
  (is (= 3 (config/get-in [{:a 1 :b 3} {:b 2}] [:b])))

  (is (nil? (config/get-in [{:a 1}] [:a :b :c])))
  (is (= 1 (config/get-in [{:a {:b {:c 1}}}] [:a :b :c])))
  (is (= 1 (config/get-in [{:a {:b {:d 3}}}
                           {:a {:b {:c 1}}}]
                          [:a :b :c]))))
