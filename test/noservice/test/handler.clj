(ns noservice.test.handler
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   [clojure.test :refer [deftest is testing use-fixtures]]
   [crypto.random :as random]
   [kerodon.core :as kc :refer [fill-in follow-redirect press session]]
   [kerodon.test :refer [attr? has regex? status? text? value?]]
   [murphy :refer [with-final]]
   [noservice.config :refer [default-configuration]]
   [noservice.handler :refer [make-app]]
   [noservice.main :refer [write-password-to-new-file]]
   [noservice.test.util :refer [notmuch-command-path notmuch-sandbox]]
   [peridot.core :refer [request]])
  (:import
   (java.lang AutoCloseable)))

(defn close [x]
  (.close ^AutoCloseable x))

;; Move to sandbox.
(def ^:private test-passwd-file "target/tmp/noservice-testing.passwd")
(def ^:private testing-password (random/hex 256))

(use-fixtures :once
  (fn [f]
    (.mkdirs (io/as-file "target/tmp"))
    (io/delete-file test-passwd-file true)
    (assert (write-password-to-new-file testing-password test-passwd-file))
    (f)))

(defn- make-testing-app [sandbox]
  (make-app #(list* {:password-file test-passwd-file}
                    {:command
                     {:notmuch [notmuch-command-path
                                "--config" (-> sandbox :config str)]}}
                    (default-configuration))))

(defn- file-response? [r type]
  (and
   (str/starts-with? (get-in r [:response :headers "Content-Type"]) type)
   (instance? java.io.File (get-in r [:response :body]))))

(defn- visit [s res & rest]
  (apply kc/visit s (str "https://localhost" res) rest))

(defn- authorized-session [app]
  (-> (session app)
      (visit "/login")
      (fill-in "password" testing-password)
      (press "login")))

(defmacro ^:private at-login-page [r]
  `(do
     (has ~r (attr? [:body :> :form] :action "/login"))
     (has ~r (attr? [:#password] :name "password"))
     (has ~r (value? "password" ""))))

(defmacro ^:private at-hello-page [r]
  `(do
     (has ~r (regex? ".*Welcome to notmuch.*Saved searches:.*top-folder.*"))))

(defmacro ^:private follow-https-redirect [r path]
  `(let [target# (follow-redirect ~r)]
     ;; If we make it here, we know there's a location -- check other bits.
     (let [url# (java.net.URL. (get-in ~r [:response :headers "Location"]))]
       (if (every? identity [(is (= "https" (.getProtocol url#)))
                             (is (= ~path (.getPath url#)))])
         target#
         (throw (IllegalArgumentException.
                 "Previous response was not an https redirect"))))))

(deftest https-redirection
  (with-final [sandbox (notmuch-sandbox "target") :always close
               app (make-testing-app sandbox)]
    (testing "anonymous https redirections"
      (doseq [x ["/" "/invalid" "/css/screen.css" "/login"]]
        (-> (session app) (kc/visit x) (follow-https-redirect x))))
    (testing "authorized https redirections"
      (doseq [x ["/" "/invalid" "/css/screen.css" "/login"]]
        (-> (authorized-session app)
            (kc/visit (str "http://localhost" x))
            (follow-https-redirect x))))))

(deftest cookie-attributes
  (with-final [sandbox (notmuch-sandbox "target") :always close
               app (make-testing-app sandbox)
               response (-> (session app) (visit "/"))
               [cookie] (get-in response [:response :headers "Set-Cookie"])]
    (is (re-find #"Secure" cookie))
    (is (re-find #"HttpOnly" cookie))))

(deftest test-restrictions
  (with-final [sandbox (notmuch-sandbox "target") :always close
               app (make-testing-app sandbox)]

    (testing "invalid route (anonymous)"
      (-> (session app) (visit "/invalid") follow-redirect at-login-page))

    (testing "invalid route (authorized)"
      (-> (authorized-session app) (visit "/invalid") (has (status? 404))))

    (testing "public routes (anonymous)"
      (let [s (session app)]
        (is (file-response? (visit s "/css/screen.css") "text/css"))
        (is (file-response? (visit s "/img/noservice.svg") "image/svg+xml"))))

    (testing "public routes (authorized)"
      (let [s (authorized-session app)]
        (is (file-response? (visit s "/css/screen.css") "text/css"))
        (is (file-response? (visit s "/img/noservice.svg") "image/svg+xml"))))

    (testing "noservice.js route (authorized)"
      (let [s (authorized-session app)]
        (is (file-response? (visit s "/js/noservice.js") "text/javascript"))))

     (testing "restricted routes (anonymous)"
       (-> (session app) (visit "/") follow-redirect at-login-page)
       (-> (session app) (visit "/part") follow-redirect at-login-page)
       (-> (session app) (visit "/mid") follow-redirect at-login-page)
       (-> (session app) (visit "/search") follow-redirect at-login-page)
       (-> (session app) (visit "/show") follow-redirect at-login-page)
       (-> (session app) (visit "/js/noservice.js") (has (status? 302))))

     (testing "login requires anti-forgery token"
       (-> (session app)
           (request "/login" :request-method :post
                    :scheme :https
                    :params {:password testing-password})
           (has (status? 403))))))

(deftest test-login
  (with-final [sandbox (notmuch-sandbox "target") :always close
               app (make-testing-app sandbox)]

    (testing "Normal login"
      (-> (authorized-session app) follow-redirect at-hello-page))

    (testing "Login to /"
      (-> (session app)
          (visit "/")
          follow-redirect
          (fill-in "password" testing-password)
          (press "login")
          follow-redirect
          at-hello-page))

    (testing "Login to invalid"
      (-> (session app)
          (visit "/invalid")
          follow-redirect
          (fill-in "password" testing-password)
          (press "login")
          follow-redirect
          (has (text? "Not found: /invalid"))))))

(deftest test-compose
  (with-final [sandbox (notmuch-sandbox "target") :always close
               app (make-testing-app sandbox)]
    (testing "/compose is not completely broken"
      (-> (authorized-session app)
          follow-redirect
          (visit "/compose")
          (has (attr? [:form#compose-form] :action "/send-email"))))))
