((nil . ((indent-tabs-mode . nil)))
 (clojure-mode . ((eval . (put-clojure-indent 'try! 0))
                  (eval . (put-clojure-indent 'match 1))))
 (makefile-mode ((indent-tabs-mode . t))))
