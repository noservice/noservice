(ns noservice.core
  (:require
   [dommy.core :as dom]))

(defn toggle-class-visibility! [name]
  "Toggles the visibility of all elements of the named class."
  (doseq [elt (dom/sel name)]
    (dom/toggle! elt)))

(defn compose-submit []
  (.submit (dom/sel1 :#compose-form)))

(defn- duplicate-header! [node]
  (-> node
      (.cloneNode true)
      (dom/set-value! "")
      (dom/insert-after! node)))

(defn append-header! [node]
  (duplicate-header! (-> node
                         dom/parent
                         .-nextSibling
                         .-lastChild)))

(def ^{:private true :const true} backspace-key 8)
(def ^{:private true :const true} enter-key 13)
(def ^{:private true :const true} delete-key 46)

(defn handle-header-event [event]
  (let [code (or (.-keyCode event) (.-charCode event))
        target (.-target event)]
    (case code
      (backspace-key delete-key)
      (when-let [sibling (or (.-previousSibling target) (.-nextSibling target))]
        (when (= (.-value target) "")
          (.focus sibling)
          (.removeChild (.-parentNode target) target)
          (.preventDefault event)))

      enter-key
      (do
        (duplicate-header! target)
        (.focus (.-nextSibling target)))
       
      nil)))
