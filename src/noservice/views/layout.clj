(ns noservice.views.layout
  (:require
   [hiccup.page :refer [html5 include-css include-js]]
   [hiccup.util :refer [url]]
   [hiccup.form :refer [form-to submit-button text-field]]
   [hiccup.element :refer [link-to]]
   [ring.util.anti-forgery :refer [anti-forgery-field]]))

;; When auth-page? is true, show the page under the assumption the
;; request is authorized, i.e. show enabled menu items, include
;; references to resources that only an authorized request would be
;; able to access, etc.  For example, auth-page? is false for the
;; login page because the menus should be inactive, and requests for
;; noservice.js will redirect to login.

(defn- user-menu [auth-page?]
  (let [menu-link (fn [id target name]
                    (link-to {:class "menu-link" :id id} (url target) name))
        new-mail (form-to
                  (if auth-page? {} {:disabled true})
                  [:post "/find-new-mail"]
                  (anti-forgery-field)
                  (submit-button (cond-> {:id "menu-find-new-mail"}
                                   (not auth-page?) (assoc :disabled true
                                                           :class "disabled"))
                                 "New"))
        search (form-to
                (cond-> {:id "menu-search-bar"}
                  (not auth-page?) (assoc :disabled true))
                [:get "/search"]
                (text-field (cond-> {:id "menu-search-bar-input"
                                     :placeholder "Search"
                                     :autofocus true}
                              (not auth-page?) (assoc :disabled true))
                            "query" ""))
        menu [:span {:class (str "menu" (when-not auth-page? " disabled"))}
              (menu-link "menu-item-hello" "/" "Hello")
              (menu-link "menu-item-settings" "/settings" "Settings")
              (menu-link "menu-item-compose" "/compose" "Compose")
              new-mail
              search
              (menu-link "menu-item-logout" "/logout" "Logout")]]
    [:div
     menu
     [:hr {:class "menu-rule"}]]))

(defn common [{:keys [auth-page?] :or {auth-page? true}} & body]
  (html5
   [:head
    [:title "noservice"]
    [:meta {:name "viewport" :content "width=device-width"}]
    (include-css "/css/screen.css")
    (when auth-page? (include-js "/js/noservice.js"))]
   [:body
    (user-menu auth-page?)
    body]))
