(ns noservice.routes.compose
  (:require
   [clojure.edn :as edn]
   [clojure.string :as str]
   [hiccup.core :refer [h]]
   [hiccup.form :refer [form-to hidden-field text-field text-area]]
   [noservice.config :as config :refer [configured-notmuch]]
   [noservice.parse :refer [parse-reply-sexp]]
   [noservice.util :refer [html-response protected]]
   [noservice.views.layout :as layout]
   [postal.sendmail :refer [sendmail-send]]
   [postal.smtp :refer [smtp-send]]
   [ring.util.anti-forgery :refer [anti-forgery-field]])
  (:import [javax.mail.internet InternetAddress]))

(defn parse-header
  "Parse a header string into a vector of addresses."
  [header-string]
  (map str (InternetAddress/parse header-string)))

(defn get-address
  "Parse a header string for an email address sans realname."
  [header-string]
  (.getAddress ^InternetAddress (first (InternetAddress/parse header-string))))

(defn from-addresses
  "Return a vector of possible from-addresses by taking addresses in
  header-string followed by addresses in the notmuch config file."
  [config header-string]
  (let [lines #(str/split % #"\n")
        notmuch (configured-notmuch config)
        primary (-> (notmuch "config" "get" "user.primary_email") lines)
        other (-> (notmuch "config" "get" "user.other_email") lines)
        real-name (-> (notmuch "config" "get" "user.name") lines first)
        addresses (map #(str/join [real-name " <" % ">"])
                       (concat primary other))]
    (if header-string
      (-> header-string
          parse-header
          (concat addresses)
          distinct)
      addresses)))

(defn get-sender
  "Return an envelope-from address. This is taken from the noservice
  config file; if not specified the from address (sans realname) is
  used."
  [from configuration]
  (or (config/get-in (configuration) [:envelope-from])
      (get-address from)))

(defn canonicalize-header-map [header-map]
  ;; XXX Postal wants lower case for special headers :to etc
  ;; but maybe we want some other case like In-Reply-To for
  ;; non-special headers. I believe MUA should be case
  ;; insensitive so it shouldn't matter.
  (zipmap
   (map (comp keyword str/lower-case name) (keys header-map))
   (vals header-map)))

(defn format-datalist-options
  "Insert DATA as a datalist escpaing as appropriate. Data should be
  a vector of elements. If an element is a single string that is
  inserted as the content for that option; if it is a vector then the
  first item is inserted as content and the second as the desired
  value."
  [data]
  (map (fn [x]
         (if (vector? x)
           ;; By default hiccup escapes attributes but not content.
           [:option {:value (second x)} (h (first x))]
           [:option (h x)]))
       data))

(defn address-book [configuration]
  [:datalist#address-book
   (format-datalist-options
    (config/get-in (configuration) [:address-book]))])

(defn- multi-value-header [name value datalist]
  (let [lc (str/lower-case name)]
    [:div.compose-header
     [:label.header-name
      [:a {:href "#" :onclick "noservice.core.append_header_BANG_(this)"} (h name)]]
     [:span.header-value
      (text-field {:class "header-sub-value"
                   :list datalist
                   :onkeypress "noservice.core.handle_header_event(event)"}
                  (str lc "[]") value)]]))

(defn compose
  "Set up a compose form with fields pre-filled based on the arguments"
  ([configuration] (compose nil nil configuration))
  ([hdrs body configuration]
   (layout/common
    {}
    (let [headers (canonicalize-header-map hdrs)
          {:keys [to cc subject]} headers
          from (from-addresses (configuration) (:from headers))]
      (form-to {:id "compose-form"}
               [:post "/send-email"]
               (anti-forgery-field)
               (hidden-field "other-headers" (pr-str headers))
               [:datalist#noservice-from-addresses
                (format-datalist-options from)]
               (address-book configuration)
               [:div#compose-headers
                [:div.compose-header
                 [:label.header-name "From"]
                 (text-field {:class "header-value"
                              :list "noservice-from-addresses"}
                             "from" (first from))]
                (multi-value-header "To" to "address-book")
                (multi-value-header "Cc" cc "address-book")
                [:div.compose-header [:label.header-name "Subject"]
                 (text-field {:class "header-value"} "subject" subject)]]
               (text-area {:id "body"} "body" body)
               [:div#send
                [:a {:href "#" :onclick "noservice.core.compose_submit();"}
                 "Send"]])))))

(defn get-text-plain-part [part]
   (cond
    (= (:content-type part) "text/plain")
    part
    (str/starts-with? (:content-type part) "multipart/")
    (first (remove nil? (for [p (:content part)] (get-text-plain-part p))))))

;; FIXME allow reply to sender
(defn reply
  "Reply to message with ID."
  [id configuration]
  (let [notmuch (configured-notmuch (configuration))
        reply (-> (notmuch "reply" "--format=sexp" "--reply-to=all" id)
                  parse-reply-sexp)
        headers (:reply-headers reply)
        original-headers (-> reply :original :headers canonicalize-header-map)
        text (-> reply
                 :original
                 :body
                 get-text-plain-part
                 :content)
        citation-line (str "\nOn " (subs (:date original-headers) 0 16)
                           ", " (:from original-headers ) " wrote:\n\n")
        cited-text (str "> "
                        (str/join "\n> " (str/split-lines text))
                        "\n")]
    (compose headers (str "\n" citation-line cited-text) configuration)))

(defn send-message [msg configuration]
  (let [from (:from msg)
        mail-host (config/get-in (configuration) [:mail-host])
        host (:host mail-host)
        sendmail (:sendmail mail-host)]
    (cond
      (not from) [{:code -1 :message "Refusing to send"} "No From: header"]
      host [(smtp-send mail-host msg) (str "using host " host)]
      sendmail [(sendmail-send msg sendmail) (str "using configured local mail " sendmail)]
      :else [(sendmail-send msg) "using system default sendmail"])))

(defn send-email [from to cc subject body other-headers configuration]
  (layout/common
   {}
   ;; FIXME automate version in user-agent.
   (let [message (merge {:user-agent "noservice-0.01"}
                        (canonicalize-header-map (edn/read-string other-headers))
                        {:from from
                         :sender (get-sender from configuration)
                         :to (flatten (map parse-header to))
                         :cc (flatten (map parse-header cc))
                         :bcc (config/get-in (configuration) [:bcc-address])
                         :subject subject
                         :body body})
         [result info] (send-message message configuration)]
     [:div
      [:p (h (:message result))]
      [:p (h (format "(Exit code %d)" (:code result)))]
      [:p (h (format "Info: %s" info))]])))

(defn handle-compose-get [config]
  (-> (compose config) html-response protected))

(defn handle-reply-get [req config]
  (-> (reply (-> req :params :id) config) html-response protected))

(defn handle-send-email-post [req config]
  (let [{:keys [from to cc subject body other-headers]} (:params req)]
    (-> (send-email from to cc subject body other-headers config)
        html-response
        protected)))
