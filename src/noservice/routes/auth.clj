(ns noservice.routes.auth
  (:require
   [buddy.hashers :as hashers]
   [hiccup.form :refer [label form-to password-field submit-button]]
   [noservice.config :as config]
   [noservice.views.layout :as layout]
   [noservice.util :refer [html-response protected]]
   [ring.util.anti-forgery :refer [anti-forgery-field]]
   [ring.util.response :as r :refer [redirect]]))

(defn- current-time [] (.getTime (java.util.Date.)))

(defn authenticated? [req]
  (-> req :session :identity boolean))

(defn login-page
  ([] (login-page nil))
  ([error-message]
   (layout/common {:auth-page? false}
    (form-to
     [:post "/login"]
     (anti-forgery-field)
     [:div#login-box
      [:div#login-box-header [:b "noservice"]]
      [:div.login-box-row
       (label {:class "login-box-text"} :password "password")
       (password-field {:autofocus "true"} :password)
       (submit-button "login")]
      (when error-message
        [:div.login-box-row
         [:span {:class "login-box-text error-message"}
          (str error-message "; please wait 5 seconds and retry")]])]))))

(def ^{:private true} too-soon-interval 5000) ;; in milliseconds

(def ^{:private true} last-failed-attempt
  (atom (- Long/MIN_VALUE (BigDecimal. (long too-soon-interval)))))

(defn- too-soon? []
  (let [then @last-failed-attempt
        now (current-time)]
    (or (> then now)
        (< (- now then) too-soon-interval))))

(defn handle-login-post [request configuration]
  (let [password (-> request :params :password)
        request (update request :params dissoc :password)
        expected (slurp (config/get-in (configuration) [:password-file]))
        error-message (cond
                        (too-soon?) "recent login failures"
                        (not password) "password required"
                        (= password "") "password required"
                        (hashers/check password expected) nil
                        :else "invalid password")]
    (if error-message
      (do
        (reset! last-failed-attempt (current-time))
        (-> (login-page error-message) html-response))
      (let [session (-> (:session request)
                        (assoc :identity "who else would it be?")
                        (vary-meta assoc :recreate true))]
        (-> (redirect (get-in session [:noservice :redirect-after-login] "/"))
            (assoc :session session))))))

(defn handle-login-get [req]
  (if-not (authenticated? req)
    (-> (login-page) html-response protected)
    (redirect "/")))

(defn logout []
  (-> (redirect "/") (assoc :session nil)))
