(ns noservice.routes.misc
  (:require
   [noservice.config :refer [configured-notmuch]]
   [noservice.util :refer [text-response]]
   [ring.util.response :refer [redirect]]))

(defn handle-find-mail [{:keys [body headers params] :as _req} config]
  (cond
    (seq (-> params (dissoc :__anti-forgery-token)))
    (-> (text-response "/find-new-mail accepts no headers")
        (assoc :status 400))
    (seq (slurp body))
    (-> (text-response "/find-new-mail accepts no content")
        (assoc :status 400))
    :else
    (let [notmuch (configured-notmuch (config))]
      (notmuch "new")
      (redirect (headers "origin")))))
