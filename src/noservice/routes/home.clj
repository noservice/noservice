(ns noservice.routes.home
  (:require
   [clojure.string :as str]
   [hiccup.core :refer [h]]
   [hiccup.element :refer [link-to]]
   [hiccup.util :refer [url]]
   [noservice.config :as config :refer [configured-notmuch]]
   [noservice.util :refer [html-response protected]]
   [noservice.views.layout :as layout]))

;; FIXME: behavior, tests, error handling, everything...

(defn- strpart
  "Break a string into substrings of length n (or less for the first string)."
  [n s]
  (let [sn (count s)
        rsn (rem sn n)]
    (drop-while #(zero? (count %))
                (cons (subs s 0 rsn)
                      (map #(subs s % (min (+ % n) (count s)))
                           (range rsn (- sn rsn) n))))))

(defn- notmuch-formatted-counts
  "Format counts like notmuch-hello, i.e. 1 234 567."
  [config queries]
  (let [notmuch (configured-notmuch config)
        counts (try
                 (-> (notmuch {:in (str/join "\n" queries)} "count" "--batch")
                     (str/split #"\n"))
                 (catch clojure.lang.ExceptionInfo ex
                   ;; FIXME
                   (prn ex)))]
    (map #(str/join " " (strpart 3 %)) counts)))

(defn- columnated-counts [items css-id]
  (let [count-width (* 0.7 (apply max (map #(count (:count %)) items)))
        col-width (* 0.8 (apply max (map #(+ (count (:count %))
                                             (count (:name %)))
                                         items)))]
    [:div {:id css-id
           :style (str "-moz-column-width: " col-width "em;"
                       "-webkit-column-width: " col-width "em;"
                       "column-width: " col-width "em;")}
     (for [{n :count name :name query :query} items]
       [:article
        [:span.columnated-count {:style (str "min-width: " count-width "em")}
         (h n)]
        [:span.columnated-name
         (link-to (url "/search" {:query query}) (h name))]])]))

(defn- render-saved-searches [config]
  ;; FIXME: error handling (of course)
  (let [saved-searches (config/get-in config [:saved-searches])
        counts (notmuch-formatted-counts config (map :query saved-searches))
        saved-searches (map #(assoc %1 :count %2) saved-searches counts)]
    (columnated-counts saved-searches "saved-searches")))

(defn- render-all-tags [config]
  (let [notmuch (configured-notmuch config)
        tags (-> (notmuch "search" "--output=tags" "*") (str/split #"\n"))
        counts (notmuch-formatted-counts config (map #(str "is:" %) tags))
        tag-counts (map (fn [tag count]
                          {:name tag :count count :query (str "is:" tag)})
                        tags counts)]
    (columnated-counts tag-counts "all-tags")))

(defn- home-html [config]
  (layout/common
   {}
   (let [notmuch (configured-notmuch config)
         msg-count (->> (notmuch "count" "*") str/trimr (strpart 3)
                        (str/join " "))]
     (list
      [:p "Welcome to " [:a {:href "http://notmuchmail.org"} "notmuch"] "."
       "  You have " (h msg-count) " messages."]
      [:p "Saved searches:" (render-saved-searches config)]
      [:p "Recent searches:"]
      [:p "All tags:" (render-all-tags config)]))))

(defn handle-home-get [config]
  (-> (home-html config) html-response protected))

(defn handle-settings-get []
  (->> [:p "Not implemented yet."] (layout/common {}) html-response protected))
