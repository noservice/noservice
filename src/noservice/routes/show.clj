(ns noservice.routes.show
  (:require
   [clojure.edn :as edn]
   [clojure.string :as str]
   [hiccup.core :refer [h]]
   [hiccup.element :refer [link-to]]
   [hiccup.util :refer [url]]
   [noservice.config :refer [configured-notmuch]]
   [noservice.parse :refer [parse-show-sexp]]
   [noservice.util :as util :refer [html-response protected render-tags]]
   [noservice.views.layout :as layout]
   [ring.util.mime-type :refer [ext-mime-type]]
   [ring.util.response :as r]))

;; FIXME: behavior, tests, error handling, everything...

(def ^:private unique-int
  (let [msg-counter (java.util.concurrent.atomic.AtomicLong.)]
    (fn []
      (.incrementAndGet msg-counter))))

(defn- compute-mime-type
  "Compute the mime type from the content type and filename. Only
  overrides the specified type if the content-type is
  application/octet-stream."
  [type filename]
  (if (and filename (= type "application/octet-stream"))
    (ext-mime-type filename)
    type))

(defn- render-show-message-header [msg header & {:keys [name-class]}]
  (when-let [header-field ((keyword header) (:headers msg))]
    [:div [:span {:class (str/join " " ["header-name" name-class])}
           (str header ": ")]
     [:span {:class (str "header-field-" (str/lower-case header))}
      (h header-field)]]))

(defn- render-show-text-plain [text]
  (let [cited-reg-ex #"^\s*>"
        lines (str/split-lines text)
        blocks (partition-by #(if (re-find cited-reg-ex %) 't nil) lines)
        last-block (last blocks)]
    (for [block blocks]
      (let [cited? (re-find cited-reg-ex (first block))
            last? (identical? block last-block)]
        (if-not (and last? cited?)
          [:div {:class (if cited? "cited-text" "uncited-text")}
           (h (str/join "\n" block))]
          (let [trailing-citation (str "msg-trailing-citation-" (unique-int))]
            [:div {:onclick (str "noservice.core.toggle_class_visibility_BANG_(\"." trailing-citation "\");")}
             [:div {:class trailing-citation}
              [:div {:class "cited-button"} "[Trailing citation: click to toggle]"]]
             [:div {:class (str "cited-text " "trailing-citation " trailing-citation)
                    :style "display: none"}
              (h (str/join "\n" block))]]))))))

(declare render-show-part)

(defn- render-show-message-rfc822 [part msg-id]
  (let [message (:content part)
        body (message :body)]
    [:div
     [:div
      [:span {:class "part-type-marker"} "[message/rfc822]"]
      (map (partial render-show-message-header message)
           ["From" "Subject" "To" "Cc" "Date"])]
     (render-show-part msg-id body false)]))

(defn- render-show-part [msg-id part show-html?]
  ;; FIXME: charsets
  (cond
   (= "message/rfc822" (:content-type part))
   (render-show-message-rfc822 part msg-id)

   (= "text/plain" (:content-type part))
   (render-show-text-plain (:content part))

   (and (str/starts-with? (:content-type part) "text/")
        (not= "text/html" (:content-type part)))
   [:div (h (:content part))]

   (str/starts-with? (:content-type part) "multipart/")
   [:div (for [sub-part (:content part)]
           (render-show-part msg-id sub-part show-html?))]

   ;; FIXME: consider a sandbox for text/html? i.e. something like
   ;;[:iframe {:sandbox "" :seamless "" :width "100%"
   ;;          :srcdoc (:content part)}]
   :else
   (let [type (:content-type part)]
     [:div (link-to (url "/part" {:msg (str "id:" msg-id) :n (:id part)})
                    (h (if-let [filename (:filename part)]
                         (let [computed-type (compute-mime-type type filename)]
                           (if (= computed-type type)
                             (format "[%s: %s]" filename type)
                             (format "[%s: %s as %s]" filename type computed-type)))
                         (format "[%s]" type))))])))

(defn- render-show-message-content [msg]
  (let [not-summary (str "msg-not-summary-" (unique-int))
        visibility (format "display:%s;" (if (:match msg) "block" "none"))]
    [:article {:class "show-msg"}
     [:div {:class "show-headers"
            :onclick (str "noservice.core.toggle_class_visibility_BANG_(\"." not-summary "\");")}
      [:div  ; FIXME: should we have a class for the "always visible" lines?
       [:div {:class "show-msg-summary show-msg-left-pad"}
        [:span
        (h (-> msg :headers :From))
        " (" (:date_relative msg) ")"
        " (" (render-tags (:tags msg)) ")"]
        [:span
         (link-to {:class "menu-link"} (url "reply" {:id (str "id:" (:id msg))}) "reply")]]
       (render-show-message-header msg "Subject" :name-class "show-msg-left-pad")]
      ;; FIXME: handle missing headers
      [:div {:class not-summary :style visibility}
        (render-show-message-header msg "To" :name-class "show-msg-left-pad")
        (render-show-message-header msg "Cc" :name-class "show-msg-left-pad")
        (render-show-message-header msg "Date" :name-class "show-msg-left-pad")]]

     [:div {:class (str "show-body show-msg-left-pad " not-summary)
            :style visibility}
      (when-let [body (:body msg)]
        (render-show-part (:id msg) body false))]]))

(defn- render-show-message [msg]
  (render-show-message-content msg))

(defn- render-show-output [threads]
  (letfn [(render-node [n]
            (let [[msg sub-nodes] n]
              (list
               (when msg (render-show-message msg))
               (for [sub sub-nodes]
                 [:div {:class "thread-node"} (render-node sub)]))))]
    (for [thread threads]
      (for [node thread]
        (render-node node)))))

(defn- show [config query context]
  (html-response
   (layout/common
    {}
    (let [notmuch (configured-notmuch config)
          nm-show (fn [& args]
                    (-> (apply notmuch "show" "--format=sexp" "--include-html" args)
                        parse-show-sexp))
          threads (or (seq (nm-show (format "'%s and (%s)'" query context)))
                      (nm-show query))]
      ;; NB: useful when debugging.
      ;;[:pre (h (with-out-str (pprint (edn/read-string (notmuch "show" ...
      (render-show-output threads)))))

(defn- part [config msg n]
  (let [notmuch (configured-notmuch config)
        part (notmuch {:out-enc :bytes}
                      "show" "--format=raw" (format "--part=%s" n) msg)
        show-string (notmuch "show" "--format=sexp" (format "--part=%s" n) msg)
        meta-data (apply hash-map (edn/read-string show-string))
        filename (:filename meta-data)
        type (:content-type meta-data)]

    (-> (r/response (new java.io.ByteArrayInputStream part))
        (r/content-type (if-not (= n "0")
                          (compute-mime-type type filename)
                          "text/plain"))
        (util/content-disposition filename)
        (r/header "Content-Length" (count part)))))

(defn handle-mid-get
  ([id config] (protected (show (config) (str "id:" id) nil)))
  ([id n config] (protected (part (config) (str "id:" id) n))))

(defn handle-part-get [{:keys [params]} config]
  (protected (part (config) (:msg params) (:n params))))

(defn handle-show-get [{:keys [params]} config]
  (protected (show (config) (:query params) (:context params))))
