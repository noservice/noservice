(ns noservice.routes.search
  (:require
   [clojure.edn :as edn]
   [clojure.string :as str]
   [hiccup.core :refer [h]]
   [hiccup.element :refer [link-to]]
   [hiccup.util :refer [url]]
   [noservice.config :as config :refer [configured-notmuch]]
   [noservice.util :refer [html-response protected render-tags]]
   [noservice.views.layout :as layout]))

;; FIXME: behavior, tests, error handling, everything...

(defn- summarize-threads [threads context]
  [:div {:id "thread-summary-list"}
   (for [x threads]
     (let [sc {:class "thread-summary-cell"}]
       (link-to
        {:class (str/join " " (cons "thread-summary-line"
                                    ;; FIXME: encoding?
                                    (map #(str "thread-is-" %) (:tags x))))}
        (url "/show" {:query (str "thread:" (:thread x)) :context context})
        [:span sc (h (:date_relative x))]
        [:span sc (format "[%d/%d]" (:matched x) (:total x))]
        [:span {:class "thread-summary-cell author-cell"}
         (h (:authors x))]
        [:span (merge sc {:style "overflow: hidden"})
         (h (:subject x)) " (" (render-tags (:tags x)) ")"])))])

(defn- search-html [query page config]
  (let [page (try (Integer/parseInt (or page "1")) (catch NumberFormatException _))]
    (layout/common
    {}
    (if-not page
      "Error: Page not a number"
      (let [notmuch (configured-notmuch config)
            threads-per-page (config/get-in config [:threads-per-page] 20)
            offset (str "--offset=" (* (- page 1) threads-per-page))
            search (map #(let [x (apply hash-map %)] (assoc x :tags (set (:tags x))))
                        (-> (notmuch "search" "--format=sexp"
                                     (str "--limit=" threads-per-page)
                                     offset query)
                            edn/read-string))
            more? (= (count search) threads-per-page)]

        (list (summarize-threads search query)
              [:hr {:class "menu-rule"}]
              [:div {:class "menu search-footer"}
               [:span {:class "menu-link" :id "search-footer-newer"}
                (if (> page 1)
                  (link-to {:title "Newer Messages"}
                           (url "/search" {:query query :page (dec page)})
                           (h "Newer"))
                  (h "Newer"))]
               [:span {:class "menu-link" :id "search-footer-older"}
                (if more?
                  (link-to {:title "Older Messages"}
                           (url "/search" {:query query :page (inc page)})
                           (h "Older" ))
                  (h "Older"))]]))))))

(defn handle-search-get [req config]
  (let [{:keys [query page]} (:params req)]
    (-> (search-html query page (config)) html-response protected)))
