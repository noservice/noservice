(ns noservice.util
  (:require
   [clojure.java.shell :refer [sh]]
   [clojure.string :as str]
   [hiccup.core :refer [h]]
   [ring.util.response :as r])
  (:import
   (java.io File)
   (java.nio.file Path)))

(defn remove-vals
  "Returns a map containing only the entries in map for which (pred
  value) is true"
  [pred map]
  (into {} (remove (comp pred second) map)))

(defn exo [opts-or-arg & args]
  (let [[opts args] (if (map? opts-or-arg)
                      [opts-or-arg args]
                      [nil (cons opts-or-arg args)])
        {:keys [check?] :or {check? true}} opts
        sh-opts (->> (select-keys opts [:in :in-enc :out-enc :env :dir])
                     (apply concat))
        res (apply sh (concat args sh-opts))]
    (when (and check? (not= 0 (:exit res)))
      (throw (ex-info (format "%s command failed with %d status"
                              (first args) (:exit res))
                      {})))
    (:out res)))

(defn rm-rf [& paths]
  (doseq [path paths]
    (cond
      (instance? String path) true
      (instance? Path path) true
      (instance? File path) true
      :else (throw (IllegalArgumentException. (str "Invalid rm-rf argument:" path)))))
  ;; Q: always print stderr stdout somewhere?
  ;; Life's too short
  (apply exo "rm" "-rf" (map str paths)))

(defn render-tags [tags]
  (interpose " " (for [tag (sort tags)]
                   ;; FIXME: do proper (html5 at least) tag name mangling.
                   ;; and maybe something else entirely...
                   [:span {:class (str "tag tag-" tag)}
                    (if (= tag "flagged")
                      "&#x2605;"  ; star
                      (h tag))])))

;; This content-disposition handling should roughly follow these
;; recommendations http://greenbytes.de/tech/webdav/rfc6266.html#rfc.section.D
;;
;; The current approach is lazy -- quash anything to undersore in
;; filename that's not one of the "normal" quoted-string characters,
;; and just hex encode filename* completely.

(def ^:private invalid-char-rx
  (re-pattern (str "[^"
                   (apply str (map #(format "\\x%02x" %)
                                   (concat [32 33] (range 35 127))))
                   "]")))

(defn- mangle-to-quoted-str [s]
  (str/replace s invalid-char-rx "_"))

(defn text-response [body]
  (-> (r/response body) (r/content-type "text/plain; charset=utf-8")))

(defn html-response [x]
  (-> x r/response (r/content-type "text/html; charset=utf-8")))

(defn protected [res]
  (r/header res
            "Cache-Control" "no-cache, no-store, must-revalidate, max-age=0"))

(defn content-disposition [response ^String filename]
  (if-not filename
    (r/header response "Content-Disposition" "inline;")
    (r/header response
              "Content-Disposition"
              (format "inline; filename=\"%s\"; filename*='UTF-8'%s;"
                      (mangle-to-quoted-str filename)
                      (apply str (for [i (.getBytes filename "utf-8")]
                                   (format "%%%02x" i)))))))
