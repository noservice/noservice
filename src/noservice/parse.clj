(ns noservice.parse
  (:require
   [clojure.edn :as edn]
   [clojure.string :as str])
  (:import
   [java.util Date]))

;; These parse functions rework the incoming notmuch sexp to be more
;; clojure-friendly, converting (:foo bar) to a real #{:foo bar} map,
;; etc.  At the moment, these only go as deep as I've needed -- so
;; below a certain point, you may encounter the raw sexp->edn
;; conversion.  FIXME: perhaps remap sexp t to true and null to false.

(defn- parse-encstatus [[form]] ;; Strip the outer []
  (apply hash-map form))

(defn- parse-signature [form]
  (let [s (apply hash-map form)]
    (conj s
          (when-let [x (:created s)] [:created (Date. (long x))])
          (when-let [x (:expires s)] [:expires (Date. (long x))]))))

(defn- parse-show-part [form]
  (let [part (apply hash-map form)
        part (conj
              part
              (when-let [encstatus (:encstatus part)]
                [:encstatus (parse-encstatus encstatus)])
              (when-let [sigstatus (:sigstatus part)]
                [:sigstatus (map parse-signature sigstatus)]))]
    (cond
     (= "message/rfc822" (:content-type part))
     (let [[{:keys [headers body]}] (:content part)]
       (assoc part :content {:headers (apply hash-map headers)
                             :body (parse-show-part (first body))}))
     (str/starts-with?(:content-type part) "multipart/")
     (assoc part :content (for [part (:content part)]
                            (parse-show-part part)))
     :else part)))

(defn- parse-show-message [form]
  (let [msg (apply hash-map form)]
    (conj msg
          [:match (= (:match msg) 't)]
          [:timestamp (Date. (long (:timestamp msg)))]
          [:tags (apply hash-set (:tags msg))]
          [:headers (apply hash-map (:headers msg))]
          (when-let [[body] (:body msg)]  ;; Strip the outer []
            [:body (parse-show-part body)]))))

(defn- parse-reply-message [form]
  (let [msg (apply hash-map form)
        original (apply hash-map (:original msg))
        original (conj original
                       (when-let [[body] (:body original)]  ;; Strip the outer []
                         [:body (parse-show-part body)])
                       (when-let [original-headers (:headers original)]
                         [:headers (apply hash-map original-headers)]))]
    (conj msg
          [:original original]
          [:reply-headers (apply hash-map (:reply-headers msg))])))

(defn- parse-thread-node [form]
  (let [[msg nodes] form]
    [(when msg (parse-show-message msg))
     (for [node nodes]
       (parse-thread-node node))]))

(defn- parse-thread-set [form]
  (for [thread form]
    (for [node thread]
      (parse-thread-node node))))

(defn parse-part-sexp [s]
  (parse-show-part (edn/read-string s)))

(defn parse-show-sexp [s]
  (parse-thread-set (edn/read-string s)))

(defn parse-reply-sexp [s]
  (parse-reply-message (edn/read-string s)))
