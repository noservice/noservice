(ns noservice.config
  (:require
   [noservice.util :refer [exo]])
  (:refer-clojure :exclude [get-in]))

(def ^{:private true} default-config
  {:client-certs true
   :sni-host-check? true
   :listen {:address "localhost"
            :port 3000
            :ssl-port 3443}
   :password-file "noservice.passwd"
   :access-log {:path-prefix "./access-log." :retain-days 7}
   :command {:notmuch ["notmuch"]}
   :saved-searches
   [{:name "top-folder" :query "folder:"}
    {:name "today" :query  "date:1d.."}
    {:name "past 7 days" :query  "date:7d.."}
    {:name "past 31 days" :query  "date:31d.."}]})

(defn default-configuration []
  [default-config])

(def ^{:arglists '([configuration ks] [configuration ks not-found])
       :doc "Returns the first value associated with ks (a sequence of
  keys) found in configuration (a sequence of nested associative
  structures). Returns nil if the key is not present, or the not-found
  value if supplied.  Similar to clojure.core/get-in."}
  get-in
  (let [nope (Object.)]
    (fn get-in
      ([configuration ks] (get-in configuration ks nil))
      ([configuration ks not-found]
       (if-not (seq configuration)
         not-found
         (let [v (clojure.core/get-in (first configuration) ks nope)]
           (if-not (identical? v nope)
             v
             (recur (rest configuration) ks not-found))))))))

(defn configured-notmuch [config]
  (assert (every? map? config))
  (fn notmuch
    ([] (notmuch nil))
    ([opts-or-arg & args]
     (let [cmd (get-in config [:command :notmuch])]
       (assert cmd)
       (if (or (nil? opts-or-arg) (map? opts-or-arg))
         (apply exo opts-or-arg (concat cmd args))
         (apply exo (concat cmd [opts-or-arg] args)))))))
