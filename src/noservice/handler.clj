(ns noservice.handler
  (:require
   [clojure.core.match :refer [match]]
   [clojure.string :as str]
   [hiccup.middleware :refer [wrap-base-url]]
   [noservice.config :as config]
   [noservice.routes.auth :as auth :refer [authenticated?]]
   [noservice.routes.compose :as compose]
   [noservice.routes.home :as home]
   [noservice.routes.misc :as misc]
   [noservice.routes.search :as search]
   [noservice.routes.show :as show]
   [noservice.util :refer [text-response]]
   [ring.middleware.defaults :refer [secure-site-defaults wrap-defaults]]
   [ring.middleware.session-timeout :refer [wrap-idle-session-timeout]]
   [ring.util.request :refer [request-url]]
   [ring.util.response :as r :refer [redirect resource-response]])
  (:import
   (java.net HttpURLConnection)))

(defn- redirect-to-login [request]
  (-> (redirect "/login")
      (assoc :session (assoc-in (:session request)
                                [:noservice :redirect-after-login]
                                (request-url request)))))

(defn invalid-method-response [uri method allowed]
  (-> (format "Invalid %s method in request for %s" (name method) uri)
      r/response
      (r/content-type "text/plain; charset=utf-8")
      (r/status HttpURLConnection/HTTP_BAD_METHOD)
      (r/header "Allow" (str/join "," (map #(-> % name str/upper-case)
                                           allowed)))))

(defn handle-public-resource-req [method uri]
  (case method
    :get (resource-response (str "public/" uri))
    (invalid-method-response uri method [:get])))

(defn responder
  [config]
  (fn handle-request [{:keys [request-method uri] :as req}]
    (case uri
      ;; some static resources from resources/public/*
      ("/css/screen.css"
       "/favicon.ico"
       "/img/noservice.svg") (handle-public-resource-req request-method uri)

      "/js/noservice.js" (if (authenticated? req)
                           (handle-public-resource-req request-method uri)
                           (redirect-to-login req))

      (letfn [(invalid-method [allowed]
                (invalid-method-response uri request-method allowed))
              (authorized [f]
                (if (authenticated? req)
                  (f)
                  (redirect-to-login req)))]
        (match [(str/split uri #"/") request-method]
          ;; No tests for show and/or mid?
          [[] :get] (authorized #(home/handle-home-get (config)))
          [[] _] (invalid-method [:get])

          [["" "compose"] :get] (authorized #(compose/handle-compose-get config))
          [["" "compose"] _] (invalid-method [:get])

          [["" "login"] :get] (auth/handle-login-get req)
          [["" "login"] :post] (auth/handle-login-post req config)
          [["" "login"] _] (invalid-method [:get :post])

          [["" "logout"] :get] (auth/logout)
          [["" "logout"] _] (invalid-method [:get])

          [["" "mid" id] :get] (authorized #(show/handle-mid-get id config))
          [["" "mid" _] _] (invalid-method [:get])

          [["" "mid" id n] :get] (authorized #(show/handle-mid-get id n config))
          [["" "mid" _ _] _] (invalid-method [:get])

          [["" "find-new-mail"] :post] (authorized #(misc/handle-find-mail req config))
          [["" "find-new-mail"] _] (invalid-method [:post])

          [["" "part"] :get] (authorized #(show/handle-part-get req config))
          [["" "part"] _] (invalid-method [:get])

          [["" "reply"] :get] (authorized #(compose/handle-reply-get req config))
          [["" "reply"] _] (invalid-method [:get])

          [["" "search"] :get] (authorized #(search/handle-search-get req config))
          [["" "search"] _] (invalid-method [:get])

          [["" "send-email"] :get] (redirect "/compose")
          [["" "send-email"] :post] (authorized #(compose/handle-send-email-post req config))
          [["" "send-email"] _] (invalid-method [:get :post])

          [["" "settings"] :get] (authorized #(home/handle-settings-get))
          [["" "settings"] _] (invalid-method [:get])

          [["" "show"] :get] (authorized #(show/handle-show-get req config))
          [["" "show"] _] (invalid-method [:get])

          :else (authorized #(-> (text-response (str "Not found: " uri))
                                 (r/status 404))))))))

(defn make-app [configuration]
  (let [ring-defaults (-> secure-site-defaults
                          (assoc-in [:security :ssl-redirect]
                                    {:ssl-port
                                     (config/get-in (configuration)
                                                    [:listen :ssl-port])})
                          (assoc-in [:static :resources] nil))]
    (-> (responder configuration)
        ;; https://github.com/ring-clojure/ring-defaults/blob/master/src/ring/middleware/defaults.clj
        (wrap-idle-session-timeout {:timeout (* 60 30)
                                    :timeout-handler (fn [req]
                                                       (redirect (request-url req)))})
        (wrap-defaults ring-defaults)
        (wrap-base-url))))
