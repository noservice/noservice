(ns noservice.main
  (:require
   [buddy.hashers :as hashers]
   [clojure.edn :as edn]
   [clojure.java.io :refer [delete-file as-file]]
   [murphy :refer [try!]]
   [noservice.config :as config :refer [default-configuration]]
   [noservice.handler :refer [make-app]]
   [ring.adapter.jetty :refer [run-jetty]])
  (:import
   [clojure.lang ExceptionInfo PersistentVector]
   [java.nio ByteBuffer]
   [java.nio.channels SeekableByteChannel]
   [java.nio.file FileAlreadyExistsException Files StandardOpenOption]
   [java.nio.file.attribute PosixFilePermissions]
   [java.util HashSet]
   [org.eclipse.jetty.server CustomRequestLog RequestLogWriter Server]
   [org.eclipse.jetty.server.handler.gzip GzipHandler])
  (:gen-class))

(defn- exit [status]
  (throw (ex-info "" {:kind :noservice/exit :status status})))

(defn- msg [stream & items]
  (binding [*out* stream]
    (doseq [i items]
      (print i))
    (flush)))

(defn- usage [stream]
  (msg stream
       "Usage:
  noservice server
      [--config EDN_FILE]
      [--client-certs] [--no-client-certs]
      [--sni-host-check] [--no-sni-host-check]
  noservice passwd
      [FILE]\n"))

(defn- misuse []
  (usage *err*)
  (exit 2))

(defn- help
  ([] (usage *out*) 0)
  ([_ & _] (misuse)))

(defn- private-file ^SeekableByteChannel [path]
  (let [nio-path (.toPath (as-file path))
        ;; https://clojure.atlassian.net/browse/CLJ-1929
        ^PersistentVector options [StandardOpenOption/CREATE_NEW
                                   StandardOpenOption/APPEND]
        options (HashSet. options)
        perms (PosixFilePermissions/fromString "rw-------")
        attr (PosixFilePermissions/asFileAttribute perms)]
    (Files/newByteChannel nio-path options (into-array [attr]))))

(defn write-password-to-new-file [password path]
  (when-let [^SeekableByteChannel password-file
             (try (private-file path)
                  (catch FileAlreadyExistsException _
                    false))]
    (with-open [password-file password-file]
      (try
        ;; FIXME: handle partial writes
        (.write password-file
                (-> ^String (hashers/derive password)
                    .getBytes ByteBuffer/wrap))
        (catch Exception ex
          (delete-file path true)
          (throw ex))))))

(defn- read-password []
  (apply str (.. System console readPassword)))

(defn passwd
  ([] (passwd "noservice.passwd"))
  ([password-file-path]
   (let [exists-msg "File already exists; delete it for a new password\n"]
       (if (.exists (as-file password-file-path))
         (do (msg *err* exists-msg) 2)
         (let [_ (do (print "New noservice password: ") (flush))
               password (read-password)
               _ (do (print "Retype new noservice password: ") (flush))
               retype (read-password)]
           (if-not (= password retype)
             (do (msg *err* (format "Sorry, passwords do not match\n")) 2)
             (if (write-password-to-new-file password password-file-path)
               0
               (do (msg *err* exists-msg) 2))))))))

(defn- add-request-logging [^Server server path-prefix retain-days]
  (as-> (str path-prefix "yyyy_mm_dd")
      x
    (doto (RequestLogWriter. x)
      (.setFilenameDateFormat "yyyy-MM-dd")
      (.setRetainDays retain-days)
      (.setAppend true)
      (.setTimeZone "GMT"))
    (CustomRequestLog. x CustomRequestLog/EXTENDED_NCSA_FORMAT)
    (.setRequestLog server x)))

(defn- add-response-compression [^Server server]
  (let [types  (into-array String ["application/clojure"
                                   "application/javascript"
                                   "application/json"
                                   "application/xhtml+xml"
                                   "image/svg+xml"
                                   "text/css"
                                   "text/html"
                                   "text/javascript"
                                   "text/plain"
                                   "text/xml"])]
    (.setHandler server
                 (doto (GzipHandler.)
                   (.setIncludedMimeTypes types)
                   (.setHandler (.getHandler server))))))

(defn- process-server-args
  [command-line-args]
  (loop [config (default-configuration)
         args command-line-args]
    (if (seq args)
      (let [[opt & remainder] args]
        (case opt
          "--config"
          (if-let [[filename & remainder] remainder]
            (recur (cons (edn/read-string (slurp filename)) config)
                   remainder)
            (misuse))
          "--client-certs"
          (recur (cons {:client-certs true} config) remainder)
          "--no-client-certs"
          (recur (cons {:client-certs false} config) remainder)
          "--sni-host-check"
          (recur (cons {:sni-host-check? true} config) remainder)
          "--no-sni-host-check"
          (recur (cons {:sni-host-check? false} config) remainder)
          (misuse)))
      {:config config})))

(defn server
  [& command-line-args]
  (let [{:keys [config]} (process-server-args command-line-args)
        configure (fn [server]
                    (add-response-compression server)
                    (when (config/get-in config [:access-log])
                      (add-request-logging
                       server
                       (config/get-in config [:access-log :path-prefix])
                       (config/get-in config [:access-log :retain-days]))))
        sni? (config/get-in config [:sni-host-check?])
        opts {:host (config/get-in config [:listen :address])
              :port (config/get-in config [:listen :port])
              :ssl-port (config/get-in config [:listen :ssl-port])
              :keystore "noservice.p12"
              :key-password (slurp "noservice.keypass")
              :configurator configure
              :sni-host-check? sni?}
        keystore (.getAbsolutePath (as-file "noservice.p12"))
        truststore (.getAbsolutePath (as-file "truststore.p12"))
        client-certs (config/get-in config [:client-certs])
        client-opts (when (and client-certs (.exists (as-file truststore)))
                      {:client-auth :need
                       :truststore truststore
                       :trust-password (slurp "noservice.keypass")})]

    (msg *err* "Using keystore " (pr-str keystore) ".\n")
    (if (and client-certs (not client-opts))
      (do
        (msg *err*
             "Can't read client certificates from " (pr-str truststore) ".\n"
             "Specify --no-client-certs (potentially INSECURE) to disable "
             "this check.\n")
        (exit 2))
      (do
        (if client-certs
          (msg *err* "Using truststore " (pr-str truststore) ".\n")
          (msg *err* "Client-certs disabled (potentially INSECURE)\n"))
        (when-not sni?
          (msg *err* "SNI host check disabled\n"))
        (msg *err* (format "Available at http://%s:%s and https://%s:%s\n"
                           (:host opts) (:port opts)
                           (:host opts) (:ssl-port opts)))
        (run-jetty (make-app #(do config)) (merge opts client-opts))
        0))))

(defn- main
  ([] (misuse))
  ([command & opts]
     (case command
       "help" (help opts)
       "server" (apply server opts)
       "passwd" (apply passwd opts)
       (misuse))))

(defn -main [& args]
  (System/exit (try!
                 (apply main args)
                 (catch ExceptionInfo ex
                   (let [{:keys [kind status]} (ex-data ex)]
                     (case kind
                       :noservice/exit status
                       (throw ex))))
                 (finally
                   (shutdown-agents)
                   (binding [*out* *err*] (flush))
                   (flush)))))
