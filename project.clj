(defproject noservice "0.1.0-SNAPSHOT"
  :description "unsafe, toy web interface for notmuch"
  :url "https://git.debian.org/?p=users/rlb/noservice.git"
  :licenses [{:name "GNU Lesser General Public License, version 2.1 or newer"
              :url "https://www.gnu.org/licenses/lgpl-2.1.html"}
             {:name "Eclipse Public License 1.0 or newer"
              :url "http://www.eclipse.org/legal/epl-v10.html"}]
  :min-lein-version "2.4.3"  ;; After switch to https for the central repo
  :pedantic? :abort
  ;; Security
  :exclusions [com.fasterxml.jackson.core/jackson-databind]
  :dependencies [[buddy/buddy-hashers "2.0.167"]
                 [hiccup "1.0.5"]
                 [murphy "0.5.2"]
                 [org.clojure/clojure "1.11.1"]
                 [org.clojure/clojurescript "1.11.121"
                  :exclusions [com.fasterxml.jackson.core/jackson-core]]
                 [org.clojure/core.match "1.0.1"]
                 [prismatic/dommy "1.1.0"]
                 [ring "1.11.0"]
                 [ring/ring-defaults "0.4.0"]
                 [ring/ring-anti-forgery "1.3.0"
                  :exclusions [crypto-equality crypto-random]]
                 [ring/ring-session-timeout "0.3.0"]
                 [ring/ring-ssl "0.3.0"]
                 [com.draines/postal "2.0.5" :exclusions [commons-codec]]
                 [com.sun.mail/javax.mail "1.6.2"]

                 ;; Conflict resolution
                 [clj-stacktrace "0.2.8"]]
  ;; FIXME: any way for clean to clean ./nvd (right now ./check-nvd does it)
  :plugins [[lein-cljsbuild "1.1.8"]]
  :cljsbuild {:builds [{:source-paths ["cljs"]
                        :compiler {:output-to "target/pub/js/noservice.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]}
  :hooks [leiningen.cljsbuild]
  :main ^:skip-aot noservice.main
  :target-path "target/%s"
  :profiles
  {:dev
   {:dependencies [[crypto-random "1.2.1" :exclusions [commons-codec]]
                   [kerodon "0.9.1" :exclusions [ring/ring-codec]]]}

   :eastwood {:plugins [[jonase/eastwood "1.2.3"]]}
   :kondo {:dependencies [[clj-kondo "2023.12.15"]
                          ;; conflict resolution
                          [com.cognitect/transit-java "1.0.371"]]}

   :uberjar
   {:aot :all
    :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}}

  :aliases
  {"eastwood" ["with-profile" "+eastwood" "eastwood"]
   "kondo" ["with-profile" "+kondo" "run" "-m" "clj-kondo.main"]})
