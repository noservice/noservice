(defproject nvd "0.1.0-SNAPSHOT"
  ;; https://github.com/rm-hull/lein-nvd#avoiding-classpath-interference
  ;; Config is in config.edn
  ;; Run checks from ../ via ./check-nvd
  :description "Helper that runs lein-nvd checks outside of the main project"
  :url "https://git.debian.org/?p=users/rlb/noservice.git"
  :licenses [{:name "GNU Lesser General Public License, version 2.1 or newer"
              :url "https://www.gnu.org/licenses/lgpl-2.1.html"}
             {:name "Eclipse Public License 1.0 or newer"
              :url "http://www.eclipse.org/legal/epl-v10.html"}]
  :dependencies [[nvd-clojure "3.2.0"]
                 [org.clojure/clojure "1.11.1"]]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
