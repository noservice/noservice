# noservice

Possibly sketchy web interface for [notmuch](http://notmuchmail.org).

While we know of no vulnerabilities when noservice is configured to
require client certificates (the default), its security has not
been evaluated by experts.

## Prerequisites

You will need a
[Java Development Kit](http://openjdk.java.net/install) (JDK) version
7 or newer, and [Leiningen](http://github.com/technomancy/leiningen)
version 2.4.3 or newer.

If you're on a fairly recent Debian system, then you should be able to
install a suitable JDK by running this (as root):

  ```sh
  apt install openjdk-11-jdk leiningen
  ```

If you don't have leiningen packages available you should be able to
copy the upstream lein script to ~/bin/ and make it executable (see
the upstream [instructions](http://leiningen.org/#install)).  After
that, you should be able to upgrade Leiningen whenever you like by
running:

  ```sh
  lein upgrade
  ```

as the same user.

Though note that the default Debian shell configuration only includes
~/bin if it exists when you log in, so you might have to log out and
log back in for a new ~/bin/ directory to be noticed.

## Running

We currently require https, so if you don't have a keystore you want
to use (doesn't everyone?), run this in the top level of the source
tree:

  ```sh
  ./create-keystore
  ```

Confusingly for our case, it asks for `first and last name`; here
that's the host name.

  ```sh
  What is your first and last name?
    [Unknown]: some.host
  ```

You'll want to set that to the (DNS) name you plan to use to access
noservice, or perhaps just for testing to `localhost`, unless you're
going to disable SNI via `--no-sni-host-check`.  If you don't disable
SNI, then whatever name you provide is the one you must use to contact
noservice.  Note that SNI explicitly forbids IP addresses.

`create-keystore` will create will create ./nostore.p12, and
./nostore.keypass -- protect both.  They should have been created with
very restrictive permissions.

However, for now assume that the key generation and https support is
potentially untrustworthy.  It needs thorough, qualified review.

To build notmuch itself, run `lein uberjar`, and if you update the
source, you'll need to run the same command again to regenerate the
jar.

We also require a password, to set it, run this:

  ```sh
  java -jar target/uberjar/noservice-0.1.0-SNAPSHOT-standalone.jar passwd
  ```

Client certificate verification is enabled by default, so all clients
must present a certificate approved by the server before they can even
reach the password prompt.
[doc/client-certs.md](./doc/client-certs.md) describes the
configuration process.

You can disable client-certificate verification with the command line
option `--no-client-certs`.  This is not a recommended configuration,
and may be insecure unless you have other security measures in place
(e.g. an ssh tunnel), but could be useful for testing.

To start a web server for the application, run:

  ```sh
  java -jar target/uberjar/noservice-0.1.0-SNAPSHOT-standalone.jar server
  ```

Alternately `lein run server ...` should work fine during development
and will automatically rebuild.

By default the server will listen on http://localhost:3000 and
https://localhost:3443, with all connections to the former
automatically redirected to the latter.

Note that right now, there is no automatic logout.  Once logged in,
you will remain logged in until you explicitly logout.  Furthermore,
logins are per-session and independent, so you can be simultaneously
logged in from multiple browsers.

To configure noservice, you can specify a config file (or files) with
--config:

  ```sh
  java -jar target/uberjar/noservice-0.1.0-SNAPSHOT-standalone.jar server --config ./config.edn
  ```

When multiple config files are specified, values in files further to
the right on the command line shadow (override) those in files further
to the left.

Here's the default config, and all items are optional:

  ```clojure
  ;; -*-clojure-*-
  {:listen {:address "localhost" :port 3000 :ssl-port 3443 }
   :access-log {:retain-days 7 :path-prefix "./access-log."}
   :saved-searches
   :command {:notmuch ["notmuch"]}
   [{:name "top-folder" :query "folder:"}
    {:name "today" :query  "date:1d.."}
    {:name "past 7 days" :query  "date:7d.."}
    {:name "past 31 days" :query  "date:31d.."}]}
  ```

If you have trouble with the lein commands you might want to try
cleaning the source tree first, i.e.:

  ```sh
  lein clean
  ```

## Sending Email

There is preliminary support for composing and replying to email. For
configuration see below.  The compose form is accessible via the
compose button in the top menu and there is a reply button on each
message in show mode.

Note that, when composing, all "to" fields in the form are joined to
make a single header, as are all "cc" fields, so you can choose
whether to add one address per field or multiple comma separated
addresses.

### Configuation

To use add the following settings to your config.edn file:

  * :mail-host specifies the host settings to use.

    For remote servers a typical setting might be the map

      ```clojure
      {:host "smtp.gmail.com"
       :user "username@gmail.com"
       :pass "password"
       :ssl :yes}
      ```

    For local servers the only recognized configuration option is
    :sendmail, which can be used to specify the sendmail path, e.g.:

        {:sendmail "/usr/bin/msmtp"}

    If neither :host (for a remote config) nor :sendmail are specified,
    then the default is to use sendmail and the path is determined by the
    SENDMAIL environment variable (if set) or by searching a hardcoded
    list of paths.

    See the postal page https://github.com/drewr/postal for a
    description of all of the options for remote servers.

  * :envelope-from specifies the address to use as the envelope-from
    address. If this is not set then the address part of the from
    address chosen will be used.

  * :bcc-address specifies an optional address to bcc all messages
    to. This is useful both as a sent mail store and as a confirmation
    the message actually got sent.

### Address book

noservice has some very basic address book support.

To use add a :address-book field to config.edn. The format should be
:address-book [a1 a2 a3...]

Each of the a's can either be an address (including real name if you
like) or a vector [alias, address]. In the latter case you type to
complete the alias but when selected the browser fills in the actual
address.

### Known Issues

* If the send fails the message is not saved anywhere. It may be
  recoverable by using back in the browser.

* There is no support for attachments.

* There is no reply-to-sender option.

* Some headers are all lower case (which is unusual but valid).

* Some versions of msmtp don't like postal's calling method: I think
  1.4.32 is OK but 1.4.31 and before *will* fail.

## Trouble

If you have any trouble, please contact us on IRC (irc.libera.chat
\#notmuch) or the notmuch mailing list (https://notmuchmail.org).

## Testing

You should be able to run all of the tests using the `notmuch` in your
path via `lein test`, as long as it runs locally, i.e. it is not an
ssh wrapper.  If it is a wrapper, then you can select a local
`notmuch` for testing by placing a symlink in `dev/bin/`.  For
example, assuming you want to use `/usr/bin/notmuch`:

    mkdir -p dev/bin
    ln -s /usr/bin/notmuch dev/bin

## License

This project is free software; you can redistribute it and/or modify
it under the terms of (at your option) either of the following two
licences:

  1) The GNU Lesser General Public License as published by the Free
     Software Foundation; either version 2.1, or (at your option) any
     later version

  2) The Eclipse Public License; either version 1.0 or (at your
     option) any later version.

Copyright © 2014 Rob Browning <rlb@defaultvalue.org>  
Copyright © 2014 Mark Walters <markwalters1009@gmail.com>  
